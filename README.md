## Yahtzee Game (Unit Test and Test Driven Development)

  This program allows a person to play a game of Yahtzee on their own.
(AKA One-Player Yahtzee) The game allows the person a chance to exit the game
after each re-roll opportunity to ensure a player is able to easily end the game
pre-maturely.  The game wil also naturally end it's self when the scoreboard
has been filled and a final score is given.

    The game operates over multiple rounds, a full game will have 13 rounds
 because it takes 13 rounds to fill up the scoreboard.  Each round will
 start with randomly generating Dice.  After the dice are rolled and displayed
 the player will be allowed to re-roll dice 3 times.  The player could choose
 at any point during the re-rolling process to skip re-rolling.
  
    After the player finishes re-rolling the player must choose a scoring option
 from the scoreboard.  The dice the player has at that time will be used to
 calculate the score for that section and then that section will dissapear
 from the scoring options for the remainder of the game.  At the end of the
 game, the scores from the different sections are accumulated to create
 different point totals for the player to view.

#### Branches
1. Yahtzee: Original code.  
2. BuildToolUnitTest: unit test on original Yahtzee code.
3. Yahtzee_TDD: test driven development method.